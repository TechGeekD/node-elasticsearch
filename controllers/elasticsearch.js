"user strict";
const client = require(__basedir + "//config/elasticsearch");
const helper = require(__basedir + "//helper");

module.exports = {
  CREATE: async function(req, res, next) {
    req.body.body["@timestamp"] = new Date().toISOString();

    let data = {
      index: req.body.index,
      type: req.body.type || "doc",
      id: req.body.id,
      body: req.body.body
    };

    helper.elasticsearch.CREATE(data, status => {
      res.json(status);
    });
  },
  INDEX: async function(req, res, next) {
    req.body.body["@timestamp"] = new Date().toISOString();

    let data = {
      index: req.body.index,
      type: req.body.type || "doc",
      body: req.body.body
    };

    helper.elasticsearch.INDEX(data, status => {
      res.json(status);
    });
  },
  UPDATE: async function(req, res, next) {
    if (req.body.body_doc && req.body.id) {
      const data = {
        index: req.body.index,
        type: req.body.type || "doc",
        id: req.body.id,
        body: {
          doc: {
            ...req.body.body_doc
          }
        }
      };

      helper.elasticsearch.UPDATE(data, status => {
        res.json(status);
      });
    } else {
      res.json({ msg: "Error: Missing Params" });
    }
  },
  DELETE: async function(req, res, next) {
    const data = {
      index: req.body.index,
      type: req.body.type || "doc",
      id: req.body.id || null
    };

    if (req.query.index) {
      helper.elasticsearch.DELETE({ index: data.index }, status => {
        res.json(status);
      });
    } else {
      helper.elasticsearch.DELETE(data, status => {
        res.json(status);
      });
    }
  },
  STATUS: async function(req, res, next) {
    helper.elasticsearch.STATUS(req, status => {
      res.json(status);
    });
  },
  TEMPLATE: function(req, res, next) {
    if (req.body.properties && req.body.index_patters) {
      const data = {
        index_patterns: req.body.index_patters,
        settings: {
          number_of_shards: req.body.settings_number_of_shards || 1,
          index: {
            refresh_interval: req.body.settings_index_refresh_interval || "10s",
            number_of_shards: req.body.settings_index_number_of_shards || 1,
            number_of_replicas: req.body.settings_index_number_of_replicas || 0
          }
        },
        mappings: {
          _doc: {
            _source: {
              enabled: req.body._source_enabled || false
            },
            properties: {
              "@timestamp": {
                type: "date",
                ignore_malformed: true
              },
              ...req.body.properties
            }
          },
          _type: req.body._type || "_doc"
        }
      };

      helper.elasticsearch.TEMPLATE(data, status => {
        res.json(status);
      });
    } else {
      res.json({
        msg: "Error: Missing Params"
      });
    }
  }
};
