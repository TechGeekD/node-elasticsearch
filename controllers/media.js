"use strict";

const helper = require(__basedir + "//helper");

module.exports = {
  UPLOAD: async function(req, res, next) {
    console.log("upload", req.file);

    let data = req.file;

    helper.media.UPLOAD(data, status => {
      res.json(status);
    });
  }
};
