"user strict";
const elasticsearch = require("elasticsearch");

const client = new elasticsearch.Client({
  host: {
    host: "localhost",
    auth: "elastic:48122112",
    protocol: "http",
    port: 9200
  },
  log: "trace"
});

// client.ping(
//   {
//     requestTimeout: 30000
//   },
//   function(error) {
//     if (error) {
//       console.error("elasticsearch cluster is DOWN!");
//     } else {
//       console.log("elasticsearch cluster is UP!");
//     }
//   }
// );

module.exports = client;
