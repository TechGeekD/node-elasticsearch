"user strict";
var express = require("express");
const multer = require("multer");

const mediaController = require(__basedir + "/controllers/media");
const router = express.Router();

let _multer = multer();

router.post(
  "/UPLOAD",
  _multer.single('csv'),
  mediaController.UPLOAD
);

module.exports = router;
