"user strict";
var express = require("express");
var router = express.Router();

const elasticController = require(__basedir + "/controllers/elasticsearch");

/* GET home page. */
router.get("/", function(req, res, next) {
  console.log("HOME");
  res.render("index", { title: "Express" });
});
router.get("/STATUS", elasticController.STATUS);
router.post("/CREATE", elasticController.CREATE);
router.post("/INDEX", elasticController.INDEX);
router.put("/UPDATE", elasticController.UPDATE);
router.delete("/DELETE", elasticController.DELETE);

module.exports = router;
