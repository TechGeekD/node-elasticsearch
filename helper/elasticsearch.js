"user strict";
const client = require(__basedir + "//config/elasticsearch");

module.exports = {
  CREATE: async function(req, cb) {
    console.log("CREATE", JSON.stringify(req, null, 4));

    const exists = await client.exists(req);

    if (!exists) {
      await client.create(req, function(err) {
        if (!err) {
          cb({ msg: "Success" });
        } else {
          console.log("CREATE_ERR", err);
          cb({ msg: "Error" });
        }
      });
    } else {
      cb({ msg: "Already Exist" });
    }
  },
  INDEX: async function(req, cb) {
    console.log("INDEX", JSON.stringify(req, null, 4));

    await client.index(req, function(err) {
      if (!err) {
        cb({ msg: "Success" });
      } else {
        console.log("INDEX_ERR", err);
        cb({ msg: "Error" });
      }
    });
  },
  UPDATE: async function(req, cb) {
    console.log("UPDATE", JSON.stringify(req, null, 4));

    await client.update(req, function(err) {
      if (!err) {
        cb({ msg: "Success" });
      } else {
        console.log("UPDATE_ERR", err);
        cb({ msg: "Error" });
      }
    });
  },
  DELETE: async function(req, cb) {
    console.log("DELETE", JSON.stringify(req, null, 4));

    if (!req.id) {
      await client.indices.delete(req, function(err) {
        if (!err) {
          cb({ msg: "Success" });
        } else {
          console.log("DELETE_ERR", err);
          cb({ msg: "Error" });
        }
      });
    } else {
      await client.delete(req, function(err) {
        if (!err) {
          cb({ msg: "Success" });
        } else {
          console.log("DELETE_ERR", err);
          cb({ msg: "Error" });
        }
      });
    }
  },
  STATUS: async function(req, cb) {
    console.log("STATUS");

    client.ping(
      {
        requestTimeout: 30000
      },
      function(err) {
        if (!err) {
          console.log("elasticsearch cluster is UP!");
          cb({ msg: "elasticsearch cluster is UP" });
        } else {
          console.error("elasticsearch cluster is DOWN!");
          cb({ msg: "elasticsearch cluster is DOWN" });
        }
      }
    );
  },
  TEMPLATE: function(req, cb) {
    console.log("TEMPLATE", JSON.stringify(req, null, 4));

    client.indices.putTemplate(req, function(err) {
      if (!err) {
        cb({ msg: "Success" });
      } else {
        console.log("TEMPLATE_ERR", err);
        cb({ msg: "Error" });
      }
    });
  }
};
