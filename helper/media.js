"user strict";

const csv = require("csvtojson");

module.exports = {
  UPLOAD: async function(req, cb) {
    csv()
      .fromString(req.buffer.toString("utf8"))
      .then(jsonObj => {
        jsonObj.forEach((e, i) =>
          Object.keys(e).forEach(e => {
            let nan = isNaN(jsonObj[i][e]);
            if (!nan) {
              jsonObj[i][e] = Number(jsonObj[i][e]);
            }
          })
        );
        cb(jsonObj);
      });
  }
};
