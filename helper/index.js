const elasticsearch = require("./elasticsearch");
const media = require("./media");

module.exports = { elasticsearch, media };
